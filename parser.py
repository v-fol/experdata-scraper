import requests
import json
from itertools import islice
from bs4 import BeautifulSoup


class Scraper():

    def __init__(self, reqid, freq):
        self.headers = {
            'authority': 'search.google.com',
            'sec-ch-ua': '"Chromium";v="94", "Google Chrome";v="94", ";Not A Brand";v="99"',
            'x-same-domain': '1',
            'content-type': 'application/x-www-form-urlencoded;charset=UTF-8',
            'sec-ch-ua-mobile': '?0',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.61 Safari/537.36',
            'sec-ch-ua-platform': '"Windows"',
            'accept': '*/*',
            'origin': 'https://search.google.com',
            'x-client-data': 'CI+2yQEIo7bJAQjBtskBCKmdygEItOnKAQjq8ssBCO/yywEInvnLAQjYhMwBCNyEzAEI5oTMAQi1hcwBCNyFzAEI/4XMAQiBhswBCMzPmgIYqqnKARiOnssBGIb9ywE=',
            'sec-fetch-site': 'same-origin',
            'sec-fetch-mode': 'cors',
            'sec-fetch-dest': 'empty',
            'referer': 'https://search.google.com/',
            'accept-language': 'en-US,en;q=0.9',
            'cookie': 'OGPC=19025836-2:; OTZ=6176614_44_48_123900_44_436380; HSID=AUHGoy1pQ0TLr-6Ro; SSID=AxH5Y1JAP0cF7fliK; APISID=e3gbiG95640KcyjX/AcwCsBYFdqZndSRsE; SAPISID=D0v_Qtyr7DvMsLZC/AMrGN9h0Pjvfg5IjW; __Secure-1PAPISID=D0v_Qtyr7DvMsLZC/AMrGN9h0Pjvfg5IjW; __Secure-3PAPISID=D0v_Qtyr7DvMsLZC/AMrGN9h0Pjvfg5IjW; SID=CAiXIo5BLOU51q-5gPPe8IMXpJOxvImbNOBLfhtmK_-DCgLewhFkzrzdUp6D6WkfiI9dew.; __Secure-1PSID=CAiXIo5BLOU51q-5gPPe8IMXpJOxvImbNOBLfhtmK_-DCgLewXUKzZESRtWta9T0G6W9iA.; __Secure-3PSID=CAiXIo5BLOU51q-5gPPe8IMXpJOxvImbNOBLfhtmK_-DCgLelXpkl2J9-i6nBxhYWi-BHw.; SEARCH_SAMESITE=CgQI2JMB; 1P_JAR=2021-10-04-12; NID=511=k7cv7l_gi30QdmDRXavmko7z7WEQgOOytkIqzdaZnbLPD6pg9io0NfYq9_OlhA7nIlfkSjksGLHBSAlV-SopZZ1yu6EtcutMo6i1v7K1pnkbyDImfoMS0Fls-wtmj6s70HO474jniuo2LFHAFQUWM0Ui7qR8yMf7mbvW_aSvxY3EpxoAoI6BTUHtU4Rxd0BAa3QMlarmJN1U0nEB4Q53nm26nUiWUrbV51xV3x_0kEp957RVCChLNp4rWprCNhsTpWtO2FndAY-thR_u65ivwS7prVu3SUwJYQHnflQB17Qlf1GgZnN6v3kPSmgDR0-KThmySHOn1-f7hwP5y98wAd1PecATmfkdStp595Qdty9wngsZUn1Q9VanXsgyNRlvo7s2-w4Cus5nW96X8IJcrzWVQ7avtEFA-1cWpqqV5qs10uNC9Q; SIDCC=AJi4QfEKnLWSoxyuzDbWVBZNDtbISnFQc-d5WHAFWkJ9m93uMufbkxYBD3pIoKcluT7RIY_PXD4; __Secure-3PSIDCC=AJi4QfG0vyxKuiQbrCdhP-osmOKf4TRVyHuZIvjP4CYwBaXcW_Pg7Z0j2VlAoPhVeucCyqiabw',
        }
        self.reqid = reqid
        self.freq = freq

    def scrape(self):
        params = (
            ('rpcids', 'OFNzWe,jx7UIe,Ce7rhb,C4lTm,ueowNe,zrEBoe,YDPhmb'),
            ('f.sid', '7376402083877308405'),
            ('bl', 'boq_searchconsoleserver_20210929.03_p0'),
            ('hl', 'en'),
            ('_reqid', self.reqid),
            ('rt', 'c'),
        )

        data = {
            'f.req': f'[[["OFNzWe","[\\"{self.freq}\\",1]",null,"3"],["jx7UIe","[\\"{self.freq}\\"]",null,"4"],["Ce7rhb","[\\"{self.freq}\\",1]",null,"6"],["C4lTm","[\\"{self.freq}\\",1]",null,"10"],["ueowNe","[\\"{self.freq}\\",1]",null,"12"],["zrEBoe","[\\"{self.freq}\\"]",null,"14"],["YDPhmb","[\\"{self.freq}\\",1]",null,"15"]]]',
            'at': 'ABvcoAfyEp2N3Pc0Y6O2A5UfPX3N:1633350507380'
        }

        self.response = requests.post(
            'https://search.google.com/_/SearchConsoleUi/data/batchexecute', headers=self.headers, params=params, data=data)
        return self.response

    def __exit__(self):
        self.response.close()


scraper = Scraper(reqid='2655826', freq='Y5wnfmudnFBx8Vkd3c0pEw')
response_body = scraper.scrape()

# Getting the corect line from responce? that contains html part.
with open("body.txt", "w", encoding="utf-8") as file:
    file.write(str((response_body.content.decode('utf_8'))))
with open('body.txt', 'r', encoding="utf-8") as file:
    for line in islice(file, 13, 14, 1):
        with open("dict.txt", "w", encoding="utf-8") as file:
            file.write(str(line))

# Getting rid of shielded characters.
with open("dict.txt", "r", encoding="utf-8") as file:
    text = str(list(file.readlines())[0])
    string = ''
    prev_char = ''
    count = 0
    for char in text:
        if char == prev_char and char == "\\":
            pass
        else:
            string = string + char
        prev_char = char

    # Converting unicode characters and \n to utf
    output = string.replace(r'\u003c', '<')
    output = output.replace(r'\u003e', '>')
    output = output.replace(r'\n          ', '\n')
    output = output.replace(r'\n         ', '\n')
    output = output.replace(r'\n        ', '\n')
    output = output.replace(r'\n       ', '\n')
    output = output.replace(r'\n      ', '\n')
    output = output.replace(r'\n     ', '\n')
    output = output.replace(r'\n    ', '\n')
    output = output.replace(r'\n   ', '\n')
    output = output.replace(r'\n  ', '\n')
    output = output.replace(r'\n ', '\n')
    output = output.replace(r'\n', '\n')
    output = output.replace(r'[["wrb.fr","ueowNe","[\"', '')
    output = output.replace(r'\",true]",null,null,null,"12"]]', '')
    output = output.replace(r"\u003d{}".format('\\'), '=')

    soup = BeautifulSoup(output, 'html.parser')

    description = soup.find_all(
        "p", class_="show-more-less-text__text--less\\")
    position = soup.find_all("p", class_='experience-item__location')

    contex = {}
    count = 1
    for description_instance, position_instance in zip(description, position):
        contex[f'experience-{count}'] = {'position': position_instance.get_text().replace(
            '\n', ''), 'description': description_instance.get_text().replace('\n', '')}
        count += 1

    with open('experience.json', 'w') as file:
        json.dump(contex, file)
